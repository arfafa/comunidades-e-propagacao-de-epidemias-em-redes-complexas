# título: comunidade.py
# descrição: Programa que simula a dinâmica epidêmica dentro de uma rede
#            de acordo com o modelo SIR. Utilizando o modelo proposto por
#            Andrea Lancichietti, Santo Fortunato e Filippo Radichi
# autor: Miguel Lopes da Silva Filho
# e-mail: miguel.lopes.filho@usp.br
# data: 18/06/2018
# notas: Este programa analisa apenas a comunidade de origem da epidemia
# versão do Python: 3.6.5

import networkx as nx
from numba import jit
import numpy as np
import os

# Função responsável por alterar o parâmetro de mixagem dentro do
# arquivo 'parameters.dat' de onde é lido os parâmetros para a criação
# da rede


def altera_alfa(arg):
    file = open('parameters.dat')

    conteudo = file.readlines()

    st = conteudo[36]

    st = st.replace(st[0:3], arg)

    conteudo[36] = st

    file = open('parameters.dat', 'w')

    file.writelines(conteudo)

    file.close()


# Função que realiza a dinâmica epidêmica em si. Analisando cada nó e as
# possibilidades de se curar, ou de passar a doença adiante


@jit
def dinamica(nos, mi, beta, G, comunidade):
    aux = []

    # Este 'for' verifica se um nó infectado, demarcado por '1', acabou por se
    # curar. Caso afirmativo, ele entra para o grupo dos nós recuperados,
    # demarcados por '2'. E, caso este pertença a comunidade, a lista
    # 'comunidade' também é atualizada

    for i in range(len(nos)):
        if(nos[i] == 1):
            p = np.random.random()

            if(p < mi):
                nos[i] = 2

                if(i in comunidade.keys()):
                    comunidade[i] = 2

    # É preciso fazer uma cópia do estado atual da rede para que no próximo
    # 'for' um nó que acabou de ser infectado não possa na mesma iteração
    # infectar um vizinho

    aux = nos.copy()

    # 'For' responsável por verificar se um nó infectado transmitiu a epidemia
    # para um de seus vizinhos. Em caso afirmativo, o nó deixa de pertencer ao
    # grupo dos suscetíveis (denotados por '0') para integrar o grupo dos
    # infectados. Também é verificado se este nó pertence a comunidade,
    # em caso afirmativo, a lista 'comunidade' também é atualizada

    for i in range(len(nos)):
        if(aux[i] == 1):

            for j in G.neighbors(i):

                if(nos[int(j)] == 0):
                    p = np.random.random()

                    if(p < beta):
                        nos[int(j)] = 1

                        if(int(j) in comunidade.keys()):
                            comunidade[int(j)] = 1


def main():

    # A lista 'taxa' representa os resultados da divisão beta/mi que serão
    # analisados

    taxa = [0.2, 0.4, 0.6]

    # Este 'for' permite que se realize a dinâmica para cada para valor da
    # relação beta/mi

    for tx in taxa:

        # Variáveis que indicam os valores utilizados para algumas constantes:
        # n: número de nós da rede
        # mi: taxa de recuperação
        # beta: taxa de espalhamento da epidemia
        # O programa neste estado define 'beta' em função de 'mi' e de 'tx'
        # caso seja nescessário fazer o oposto, basta comentar estas linhas e
        # descomentar as duas de baixo

        n = 1000
        mi = 25e-5
        beta = mi*tx
        # beta = 1e-4
        # mi = beta/tx

        # Este 'for' permite que as simulações sejam rodadas para diversos
        # valores de parâmetro de mixagem

        for k in range(1, 6):

            dados = []
            # Chama a função para definir o valor do parâmetro de mixagem
            # de acordo com o valor de 'k'

            altera_alfa(str(k/10))

            # Este 'for' faz a contagem para que sejam criadas 10 redes com
            # o mesmo parâmetro de mixagem

            for contador in range(10):

                # A cada nova iteração o programa desenvolvido por
                # Andrea Lancichietti, Santo Fortunato e Filippo Radichi
                # é executado, para que assim, uma nova rede seja criada

                os.system('./benchmarck')

                # As ligações existentes entre cada nó estão explicitadas no
                # arquivo 'network.dat'. OS valores dos ligantes são subtraídos
                # de '1' apenas para facilitar a manipulação dos dados

                edges = np.loadtxt('network.dat') - 1

                # A rede é inicializada com nós nomeados de 0 até 999
                # e as ligações dadas pelo arquivo carregado logo acima

                G = nx.Graph()
                G.add_nodes_from(list(range(n)))
                G.add_edges_from(edges)

                # Este 'for' serve para que, uma vez criada a rede, itere-se
                # 10 vezes sobre a mesma, mudando apenas o nó de origem da
                # epidemia

                for count in range(10):

                    # Contagem do tempo (loops) transcorrido desde o início da
                    # simulação

                    tempo = 0

                    # Lista com o nós da rede e seus estados atuais onde:
                    # '0': suscetível
                    # '1': infectado
                    # '2': recuperado
                    # Para iniciar a dinâmica, um nó é escolhido aleatoriamente
                    # para começar infectado
                    # Nesta caso é preciso salvar o nó sorteado e carregar
                    # a relação de nós que pertence a uma determinada
                    # comunidade

                    nos = np.zeros(shape=n, dtype=int)
                    rand = np.random.randint(n)
                    nos[rand] = 1

                    comunidades = np.loadtxt('community.dat')-1

                    nos_comunidade = []

                    # Este 'for' serve para encontrar os companheiros de
                    # comunidade do nó sorteado. Para isso ele analisa
                    # a qual comunidade pertence cada nó, caso ele pertença
                    # a mesma do nó selecionado, este é adicionado na lista
                    # 'nos_comunidades'

                    for i in comunidades:
                        if(i[1] == comunidades[rand, 1]):
                            nos_comunidade.append(int(i[0]))

                    # Todos os nós da comunidade são suscetíveis quando a
                    # dinâmica começa, menos o nó sorteado. A lista
                    # 'comunidade' é formada de zeros, menos na posição
                    # igual à 'rand' onde o valor é '1'

                    comunidade = {i: 0 for i in nos_comunidade}
                    comunidade[rand] = 1

                    # O tamanho da comunidade é necessário, uma vez que, as
                    # comunidades dificilmente terão o mesmo número de nós
                    # Sendo assim, deve-se trabalhar com o valor relativo

                    tam = len(comunidade.keys())

                    dados.append([0, 1 - 1/tam, 1/tam, 0])

                    # Este 'while' indica que a dinâmica deve ocorrer
                    # enquanto o número de infectados for diferente de zero

                    while(np.count_nonzero(nos == 1) != 0):

                        # É chamada a função 'dinamica' para realizar a
                        # dinâmica epidêmica para o atual estado da rede

                        dinamica(nos, mi, beta, G, comunidade)

                        tempo += 1

                        sus = 0
                        inf = 0
                        rem = 0

                        # Este 'for' verifica o estado atual de cada nó
                        # após a 'dinamica' ter ocorrido. Acrescentando
                        # '1' para um determinado grupo dependendo do
                        # estado que o nó se encontra

                        for v in comunidade.values():
                            if(v == 0):
                                sus += 1

                            elif(v == 1):
                                inf += 1

                            elif(v == 2):
                                rem += 1

                        dados.append([tempo, sus/tam, inf/tam, rem/tam])

            dados = np.array(dados)

            # Variáveis que servem apenas para dar o nome do arquivo de saída
            # e uma pequena descrição de acordo com os parâmetros utilizados

            arq = ['dados', str(k), 'mi_fixo_taxa', str(tx)]
            arq = '_'.join(arq)
            head = ['mi fixo e beta/mi =', str(tx), ' mixing=', str(k/10)]
            # head = ['beta fixo e beta/mi=', str(tx), ' mixing=', str(k/10)]
            head = ''.join(head)

            np.savetxt(arq, dados, newline='\n', header=head)


if(__name__ == '__main__'):
    main()
